namespace WebApp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createitemtable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 128),
                        Description = c.String(maxLength: 512),
                        Date = c.DateTime(nullable: false),
                        From = c.Time(nullable: false, precision: 7),
                        To = c.Time(nullable: false, precision: 7),
                        Location = c.String(),
                        Notify = c.Int(nullable: false),
                        Label = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedBy = c.Long(nullable: false),
                        CreatedDateUtc = c.DateTime(nullable: false),
                        UpdatedBy = c.Long(nullable: false),
                        UpdatedDateUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Items");
        }
    }
}
