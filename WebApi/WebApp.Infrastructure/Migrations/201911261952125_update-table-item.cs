namespace WebApp.Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatetableitem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Items", "StatusId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Items", "StatusId");
        }
    }
}
