﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Domain.Auth
{
    public class IdentityModels
    {
        public class ApplicationUserRole : IdentityUserRole<long>
        {
        }

        public class ApplicationUserClaim : IdentityUserClaim<long>
        {
        }

        public class ApplicationUserLogin : IdentityUserLogin<long>
        {
        }

        public class ApplicationRole : IdentityRole<long, ApplicationUserRole>
        {
            public ApplicationRole() { }
            public ApplicationRole(string name) { Name = name; }

            [Required]
            public int StatusId { get; set; }
            public string Description { get; set; }

            [Required]
            public long CreatedBy { get; set; }

            [Required]
            public DateTime CreatedDateUtc { get; set; }

            public long UpdatedBy { get; set; }
            public DateTime? UpdatedDateUtc { get; set; }
        }
    }
}
