﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Domain.Entities
{
    public class Menu : Auditable
    {
        public Menu() : base()
        {
        }

        public int Id { get; set; }
        [StringLength(128)]
        [Required(ErrorMessage = "Name required.")]
        public string Name { get; set; }
        [StringLength(256)]
        public string ComponentName { get; set; }
        public string Url { get; set; }
        [StringLength(20)]
        public string Domain { get; set; }
        public int StatusId { get; set; }
        public int SortOrder { get; set; }
        [ForeignKey("ParentMenu")]
        public int? ParentMenuId { get; set; }
        public Menu ParentMenu { get; set; }
        [NotMapped]
        public List<Menu> Childrens { get; set; }
    }
}
