﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static WebApp.Domain.Auth.IdentityModels;

namespace WebApp.Domain.Entities
{
    public class UserLogIn
    {
        [Required]
        [MaxLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
    public class UserInfo : UserLogIn
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string PhoneNumber { get; set; }

        public string ProfilePicSrc { get; set; }

        [Required]
        public string ConfirmPassword { get; set; }


    }
    public class UserPasswordChange : UserLogIn
    {

        [Required]
        public string NewPassword { get; set; }

        [Required]
        public string ConfirmPassword { get; set; }

    }
    public class UserPasswordReset : UserLogIn
    {
        [Required]
        public string ConfirmPassword { get; set; }

        [Required]
        public string Token { get; set; }

    }


}
