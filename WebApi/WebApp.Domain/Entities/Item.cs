﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.Utility.Enums;

namespace WebApp.Domain.Entities
{
    public class Item : Auditable
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(128)]
        public string Title { get; set; }
        [MaxLength(512)]
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }
        public string Location { get; set; }
        public int Notify { get; set; }
        public string Label { get; set; }
        public ItemStatus StatusId { get; set; }
        public bool IsActive { get; set; }
        [NotMapped]
        public bool IsOutOfDate { get; set; } = true;
    }
}
