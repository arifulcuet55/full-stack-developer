﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.Domain.Entities;

namespace WebApp.Core.Interfaces
{
    public interface IItem : IGenericRepository<Item>
    {
        Task<List<Item>> GetsActiveAsync();
        Task<List<Item>> GetsByUserIdAsync(long userId);
        Task<Item> UpdateStatusAsyc(long id);
    }
}
