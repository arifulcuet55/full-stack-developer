﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.Domain.Entities;

namespace WebApp.Core.Interfaces
{
    public interface  IMenu : IGenericRepository<Menu>
    {
        Task<List<Menu>> GetsActiveAsync();

    }
}
