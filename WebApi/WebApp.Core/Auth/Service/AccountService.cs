﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebApp.Core.Auth.Enums;
using WebApp.Core.Auth.Store;
using WebApp.Domain;
using WebApp.Domain.Auth;
using WebApp.Infrastructure.Context;

namespace WebApp.Core.Auth.Service
{
    public class AccountService
    {
        private long _userId { get; set; }
        private UserManager<ApplicationUser, long> _userManager { get; set; }


        public AccountService()
        {
            _userId = HttpContext.Current.User.Identity.GetUserId<long>();
            _userManager = new UserManager<ApplicationUser, long>(new ApplicationUserStore(new ApplicationDbContext()));
        }

        private void GetUserTokenProvider()
        {
            var provider = new DpapiDataProtectionProvider("WebApp");
            _userManager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser, long>(provider.Create(".NET Identity"));
        }

        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<IdentityResult> CreateAsync(ApplicationUser obj, string password)
        {
            if (string.IsNullOrEmpty(obj.Name))
                throw new Exception("First name required.");

            else if (string.IsNullOrEmpty(obj.Email))
                throw new Exception("Eamil address required.");

            else if (await _userManager.Users.AnyAsync(x => x.Email == obj.Email))
                throw new Exception($"'{obj.Email}' already taken.");

            //obj.AccountStatus = (int)AccountState.Active;
            obj.ExpireDateUtc = DateTime.UtcNow.AddDays(30);
            obj.CreatedDateUtc = DateTime.UtcNow;
            obj.EmailConfirmed = false;
            obj.PhoneNumberConfirmed = false;
            obj.TwoFactorEnabled = false;
            obj.LockoutEnabled = false;
            obj.AccessFailedCount = 0;

            return await _userManager.CreateAsync(obj, password);
        }
        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public async Task<ApplicationUser> UpdateAsync(long id, ApplicationUser obj)
        {
            try
            {
                var data = await _userManager.FindByIdAsync(id);

                if (data != null)
                {
                    if (string.IsNullOrEmpty(obj.Name))
                        throw new Exception("First name required.");

                    data.Name = obj.Name;
                    data.PhoneNumber = obj.PhoneNumber;
                    data.UpdatedDateUtc = DateTime.UtcNow;
                    IdentityResult identity = await _userManager.UpdateAsync(data);
                    if (identity.Succeeded)
                    {
                        return data;
                    }
                    else
                    {
                        throw new Exception(identity.Errors.Select(c => c).ToString());
                    }
                }
                else
                {
                    throw new Exception("User doesn't found.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="currentPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public async Task<IdentityResult> ChangePasswordAsync(string currentPassword, string newPassword)
        {
            try
            {
                var appUser = _userManager.FindById(_userId);

                if (await _userManager.CheckPasswordAsync(appUser, currentPassword))
                {
                    return await _userManager.ChangePasswordAsync(_userId, currentPassword, newPassword);
                }
                else
                {
                    throw new Exception("Current password doesn't valid.");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Password reset token
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<string> GeneratePasswordResetTokenAsync(string email)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);
                this.GetUserTokenProvider();
                return await _userManager.GeneratePasswordResetTokenAsync(user.Id);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Reset password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="token"></param>
        /// <param name="password"></param>
        /// <param name="confirmPassword"></param>
        /// <returns></returns>
        public async Task<IdentityResult> ResetPasswordByTokenAsync(string email, string token, string password, string confirmPassword)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);

                if (password == confirmPassword)
                {
                    this.GetUserTokenProvider();

                    if (await _userManager.UserTokenProvider.ValidateAsync("ResetPassword", token, _userManager, user))
                    {
                        return await _userManager.ResetPasswordAsync(user.Id, token, password);
                    }
                    else
                    {
                        throw new Exception("Invalid token provided.");
                    }
                    throw new Exception("Invalid token provided.");

                }
                else
                {
                    throw new Exception("Password doesn't match.");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Signout method
        /// </summary>
        /// <returns></returns>
        public async Task<IdentityResult> SignoutAsync()
        {
            try
            {
                return await _userManager.UpdateSecurityStampAsync(_userId);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Get User by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ApplicationUser> GetById(long id)
        {
            try
            {
                ApplicationUser applicationUser = await _userManager.Users.Include(c => c.Roles).FirstOrDefaultAsync(c => c.Id == id);
                return applicationUser;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
