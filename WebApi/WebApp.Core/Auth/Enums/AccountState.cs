﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Core.Auth.Enums
{
    public enum AccountState
    {
        Active = 1,
        Inactive = 2
    }
}
