﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.Core.Auth.Store;
using WebApp.Infrastructure.Context;
using static WebApp.Domain.Auth.IdentityModels;

namespace WebApp.Core.Auth.Manager
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole, long>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole, long> store) : base(store)
        {

        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var appDbContext = context.Get<ApplicationDbContext>();
            var roleManager = new ApplicationRoleManager(new ApplicationRoleStore(appDbContext));
            return roleManager;
        }
    }
}
