﻿using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.Core.Services;
using WebApp.Domain.Auth;

namespace WebApp.Core.Auth.Provider
{
    public class AuthorizationProperties
    {
        public static async Task<AuthenticationProperties> Get(ApplicationUser user)
        {
            // put the properties here
            var menus = await new MenuService().GetsAsync();
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("email", user.Email);
            data.Add("name", user.Name);
            if(user.ProfilePicSrc != null)
            {
                data.Add("profileUrl", user.ProfilePicSrc);
            }
           
            data.Add("menus", GetSerializeObject(menus));
            return new AuthenticationProperties(data);
        }

        private static string GetSerializeObject(object obj)
        {
            try
            {
                return JsonConvert.SerializeObject(obj, Formatting.None,
                  new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            }
            catch (Exception e)
            {

                throw;
            }
           
        }
    }
}
