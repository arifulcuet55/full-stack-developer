﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WebApp.Core.Auth.Manager;
using WebApp.Core.Auth.Provider;
using WebApp.Domain.Auth;
using WebApp.Infrastructure.Context;

namespace WebApp.Core.Auth
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            UserManager<ApplicationUser, long> userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
            ApplicationUser user;
            try
            {
                user = await userManager.FindAsync(context.UserName, context.Password);
                if (user != null)
                {
                    ClaimsIdentity identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ExternalBearer); //context.Options.AuthenticationType
                    ClaimsIdentity cookiesIdentity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    //ClaimsIdentity identity = await userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ExternalBearer);

                    AuthenticationProperties properties = await AuthorizationProperties.Get(user);
                    AuthenticationTicket ticket = new AuthenticationTicket(identity, properties);

                    context.Validated(ticket);
                    context.Request.Context.Authentication.SignIn(cookiesIdentity);
                }
                else
                {
                    context.SetError("invalid_grant", "Invalid User Id or password'");
                    // context.Rejected();
                }
            }
            catch(Exception ex)
            {
                // Could not retrieve the user due to error.  
                // context.SetError("server_error");
                context.SetError(ex.Message);
                // context.Rejected();
                // return;
            }
        }

        public override async Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach(KeyValuePair<string, string> prop in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(prop.Key, prop.Value);
            }
        }
    }
}
