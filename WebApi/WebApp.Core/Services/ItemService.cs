﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApp.Core.Interfaces;
using WebApp.Domain.Entities;
using WebApp.Infrastructure.Context;
using WebApp.Utility.Enums;

namespace WebApp.Core.Services
{
    public class ItemService : GenericRepository<Item>, IItem
    {
        private readonly ApplicationDbContext _context;
        public ItemService() : this(new ApplicationDbContext()) { }
        public ItemService(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
        /// <summary>
        /// Get all active items
        /// </summary>
        /// <returns></returns>
        public async Task<List<Item>> GetsActiveAsync()
        {
            return await base.GetsAsync(x => x.IsActive == true);
        }
        /// <summary>
        /// Get Items by UserId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<List<Item>> GetsByUserIdAsync(long userId)
        {
            var result = await base.GetsAsync(x => x.CreatedBy == userId && x.IsActive == true );
            result = result.OrderBy(y=>y.Date).ThenBy(p=>p.From).ToList();
            foreach (var item in result)
            {
                if (item.Date < DateTime.Now.Date)
                {
                    item.IsOutOfDate = false;
                }
            }
            return result;
        }
        /// <summary>
        /// Set value IsActive= false
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task DeleteAsync(long id)
        {
            try
            {
                var result = await base.FirstOrDefaultAsync(x => x.Id == id);
                if (result != null)
                {
                    result.IsActive = false;
                }
                await base.EditAsync(result);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
           
        }
        /// <summary>
        /// Status update when done by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Item> UpdateStatusAsyc(long id)
        {
            try
            {
                var result = await base.FirstOrDefaultAsync(x => x.Id == id);
                if (result != null)
                {
                    result.StatusId = ItemStatus.Done;
                }
                await base.EditAsync(result);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}
