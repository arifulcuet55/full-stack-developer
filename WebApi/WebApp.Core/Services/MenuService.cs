﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebApp.Core.Interfaces;
using WebApp.Domain.Entities;
using WebApp.Utility.Enums;
using WebApp.Infrastructure.Context;

namespace WebApp.Core.Services
{
    public class MenuService : GenericRepository<Menu>, IMenu
    {
        private readonly ApplicationDbContext _context;
        public MenuService() : this(new ApplicationDbContext()) { }
        public MenuService(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<List<Menu>> GetsActiveAsync()
        {
            return await GetsAsync(x => x.StatusId == (int)MenuStatus.Active, x => x.OrderBy(e => e.SortOrder));
        }
    }
}
