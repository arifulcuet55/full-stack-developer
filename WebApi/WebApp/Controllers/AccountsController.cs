﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebApp.Domain.Auth;
using WebApp.Domain.Entities;
using WebApp.Utility.Enums;
using System.Web;
using WebApp.Core.Auth.Service;
using WebApp.Utility;
using Newtonsoft.Json;

namespace WebApp.Controllers
{
    [Authorize]
    [RoutePrefix("api/accounts")]
    public class AccountsController : ApiController
    {
        private AccountService _accountService { get; set; }

        public AccountsController()
        {
            _accountService = new AccountService();
        }

        #region User Account
        [HttpGet]
        [Route("getById/{id:long}")]
        public async Task<IHttpActionResult> GetById(long id)
        {
            try
            {
                var res = await _accountService.GetById(id);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("signup")]
        public async Task<IHttpActionResult> RegisterAsync()
        {
            try
            {
                var entity = this.SaveFile();
               
                var user = new ApplicationUser
                {
                    Name = entity.Name,
                    Email = entity.Email,
                    UserName = entity.Email,
                    BirthDate = entity.BirthDate,
                    ProfilePicSrc = entity.ProfilePicSrc
                };

                var result = await _accountService.CreateAsync(user, entity.Password);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        private UserInfo SaveFile()
        {
            string file_key = "image";
            var entity = new UserInfo();
            HttpFileCollection files = null;
            try
            {
                var model = HttpContext.Current.Request.Form.Get("model");

                entity = JsonConvert.DeserializeObject<UserInfo>(model);

                files = HttpContext.Current.Request.Files.Count > 0 ?
                        HttpContext.Current.Request.Files : null;

                if (files != null && files[file_key] != null)
                {
                    entity.ProfilePicSrc = FileHandler.SaveFile(files[file_key], FilePath.ProfilePic.ToString());
                }
                return entity;
            }
            catch (Exception ex)
            {
                if (files != null && files[file_key] != null && string.IsNullOrEmpty(entity.ProfilePicSrc))
                    FileHandler.RemoveFile(entity.ProfilePicSrc);
                throw ex;
            }

        }

        [HttpPut]
        [Route("edit/{id:long}")]
        public async Task<IHttpActionResult> EditAsync(long id, [FromBody] UserInfo userInfo)
        {
            try
            {
                var user = new ApplicationUser
                {
                    Name = userInfo.Name,
                    Email = userInfo.Email,
                    UserName = userInfo.Email,
                    BirthDate = userInfo.BirthDate
                };

                var res = await _accountService.UpdateAsync(id, user);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();

        }


        [HttpPost]
        [Route("signout")]
        public async Task<IHttpActionResult> SignOut()
        {
            try
            {
                var authheader = HttpContext.Current.Request.Headers["Authorization"];


                Request.GetOwinContext().Authentication.SignOut();
                Request.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                await _accountService.SignoutAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


        [HttpPut]
        [Route("changePassword")]
        public async Task<IHttpActionResult> ChangePassword([FromBody] UserPasswordChange obj)
        {

            try
            {
                if (obj.NewPassword != obj.ConfirmPassword)
                    throw new Exception("Password doesn't match.");

                await _accountService.ChangePasswordAsync(obj.Password, obj.NewPassword);
                return Ok();

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }


        [HttpGet]
        [AllowAnonymous]
        [Route("password/reset/token")]
        public async Task<IHttpActionResult> GetPasswordResetToken(string email)
        {
            try
            {
                var token = await _accountService.GeneratePasswordResetTokenAsync(email);
                return Ok(token);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        [AllowAnonymous]
        [Route("passwordReset")]
        public async Task<IHttpActionResult> ResetPasswordByToken([FromBody] UserPasswordReset obj)
        {
            try
            {
                await _accountService.ResetPasswordByTokenAsync(obj.Email, obj.Token, obj.Password, obj.ConfirmPassword);
                return Ok();
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }

        }

        #endregion

    }
}

