﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebApp.Core.Auth.Service;
using WebApp.Core.Interfaces;
using WebApp.Core.Services;
using WebApp.Domain.Entities;
using WebApp.Utility.Enums;

namespace WebApp.Controllers
{
    [Authorize]
    [RoutePrefix("api/item")]
    public class ItemController : ApiController
    {
        private IItem _itemService { get; set; }

        public ItemController()
        {
            _itemService = new ItemService();
        }
        [HttpGet]
        [Route("get/{id:long}")]
        public async Task<IHttpActionResult> Get(long id)
        {
            try
            {
                var result = await _itemService.GetByIdAsync(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }
        [HttpPost]
        [Route("add")]
        public async Task<IHttpActionResult> Save([FromBody] Item item)
        {
            try
            {
                item.CreatedBy = HttpContext.Current.User.Identity.GetUserId<long>();
                item.CreatedDateUtc = DateTime.UtcNow;
                item.IsActive = true;
                item.StatusId = ItemStatus.Inprogress;
                var result = await _itemService.AddAsync(item);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("gets")]
        public async Task<IHttpActionResult> Gets()
        {
            try
            {
                long userId = HttpContext.Current.User.Identity.GetUserId<long>();
                var result = await _itemService.GetsByUserIdAsync(userId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
          
        }

        [HttpPut]
        [Route("update/{id:long}")]
        public async Task<IHttpActionResult> EditAsync(long id, [FromBody] Item item)
        {
            try
            {
               
                if(id > 0)
                {
                    item.UpdatedBy = HttpContext.Current.User.Identity.GetUserId<long>();
                    item.UpdatedDateUtc = DateTime.UtcNow;
                    await _itemService.EditAsync(item);
                }
                 else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();

        }
        
        [HttpDelete]
        [Route("delete/{id:long}")]
        public async Task<IHttpActionResult> Delete(int id)
        {
            try
            {
                if (id > 0)
                {
                    await _itemService.DeleteAsync(id);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }
        [HttpGet]
        [Route("status/{id:long}")]
        public async Task<IHttpActionResult> UpdateStatusAsyc(int id)
        {
            try
            {
                if (id > 0)
                {
                    await _itemService.UpdateStatusAsyc(id);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }
    }
}