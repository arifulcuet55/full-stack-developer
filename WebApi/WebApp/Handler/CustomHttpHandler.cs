﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Handler
{
    public class CustomHttpHandler : IHttpHandler
    {
        public bool IsReusable => true;

        public void ProcessRequest(HttpContext context)
        {
            HttpResponse response = context.Response;
            response.Write("Hello From Http Handler");
        }
    }
}