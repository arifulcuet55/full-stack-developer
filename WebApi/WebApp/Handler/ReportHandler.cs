﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Utility;

namespace WebApp.Handler
{
    public class ReportHandler
    {
        public static bool ReportFile(string dataSourceName, object dataSource, string reportFileName, string reportOutFilePath, FileLayout fileLayout = FileLayout.Potrait)
        {
            try
            {
                var lr = new LocalReport();

                var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Reports/Rdlc"), reportFileName);
                if (!File.Exists(path))
                    throw new ArgumentException("report not found");

                lr.ReportPath = path;

                var rd = new ReportDataSource(dataSourceName, dataSource);
                lr.DataSources.Add(rd);

                string reportType = "pdf";
                string mimeType;
                string encoding;
                string fileNameExtension;
                string deviceInfo = string.Empty;

                if (fileLayout == FileLayout.Landscape)
                {
                    deviceInfo = "<DeviceInfo>" +
                    "  <OutputFormat>pdf</OutputFormat>" +
                    "  <PageWidth>11in</PageWidth>" +
                    "  <PageHeight>8.5in</PageHeight>" +
                    "  <MarginTop>0.5in</MarginTop>" +
                    "  <MarginLeft>.50in</MarginLeft>" +
                    "  <MarginRight>.50in</MarginRight>" +
                    "  <MarginBottom>0.50in</MarginBottom>" +
                    "</DeviceInfo>";
                }
                else
                {
                    deviceInfo = "<DeviceInfo>" +
                    "  <OutputFormat>pdf</OutputFormat>" +
                    "  <PageWidth>8.5in</PageWidth>" +
                    "  <PageHeight>11in</PageHeight>" +
                    "  <MarginTop>0.5in</MarginTop>" +
                    "  <MarginLeft>.50in</MarginLeft>" +
                    "  <MarginRight>.50in</MarginRight>" +
                    "  <MarginBottom>0.50in</MarginBottom>" +
                    "</DeviceInfo>";
                }
                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;

                renderedBytes = lr.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);

                var reportPath = HttpContext.Current.Server.MapPath(reportOutFilePath);

                renderedBytes.SaveFile(reportPath, ".pdf");

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static byte[] ReportByte(string dataSourceName, object dataSource, string reportFileName, FileLayout fileLayout = FileLayout.Potrait)
        {
            try
            {
                var lr = new LocalReport();

                var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Reports/Rdlc"), reportFileName);
                if (!File.Exists(path))
                    throw new ArgumentException("report not found");

                lr.ReportPath = path;

                var rd = new ReportDataSource(dataSourceName, dataSource);
                lr.DataSources.Add(rd);

                string reportType = "pdf";
                string mimeType;
                string encoding;
                string fileNameExtension;
                string deviceInfo = string.Empty;

                if (fileLayout == FileLayout.Landscape)
                {
                    deviceInfo = "<DeviceInfo>" +
                    "  <OutputFormat>pdf</OutputFormat>" +
                    "  <PageWidth>11in</PageWidth>" +
                    "  <PageHeight>8.5in</PageHeight>" +
                    "  <MarginTop>0.5in</MarginTop>" +
                    "  <MarginLeft>.50in</MarginLeft>" +
                    "  <MarginRight>.50in</MarginRight>" +
                    "  <MarginBottom>0.50in</MarginBottom>" +
                    "</DeviceInfo>";
                }
                else
                {
                    deviceInfo = "<DeviceInfo>" +
                    "  <OutputFormat>pdf</OutputFormat>" +
                    "  <PageWidth>8.5in</PageWidth>" +
                    "  <PageHeight>11in</PageHeight>" +
                    "  <MarginTop>0.5in</MarginTop>" +
                    "  <MarginLeft>.50in</MarginLeft>" +
                    "  <MarginRight>.50in</MarginRight>" +
                    "  <MarginBottom>0.50in</MarginBottom>" +
                    "</DeviceInfo>";
                }

                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;

                renderedBytes = lr.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);

                return renderedBytes;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    public enum FileLayout
    {
        Landscape,
        Potrait
    }
}