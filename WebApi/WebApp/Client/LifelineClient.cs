﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using WebApp.Utility;

namespace WebApp.Client
{
    public class LifelineClient
    {
        public OleDbConnection Connection { get; set; }
        OleDbDataAdapter da;
        OleDbDataAdapter da1;
        OleDbDataAdapter da2;
        DataSet ds;
        public string ctflag;
        public string cabmode = "";
        public string premium = "0.00";
        int flag = 0;
        public string PolType = "";
        public string CoverageAmnt2 = "";

        public LifelineClient()
        {
            Connection = new OleDbConnection(ConfigurationManager.ConnectionStrings["ConnectionStringRE"].ConnectionString);
        }

        #region others
        public LifelineResponse LifelineResponse(int statusCode, bool status = false, object data = null)
        {
            var statusString = "failed";
            if (status)
                statusString = "success";

            return new LifelineResponse
            {
                Status = statusString,
                StatusCode = statusCode.ToString(),
                Data = data
            };
        }
        #endregion

        public void CheckPolicy(string policyNumber)
        {
            OleDbDataAdapter da;
            DataSet ds;
            try
            {
                OleDbConnection con = new OleDbConnection(ConfigurationManager.ConnectionStrings["ConnectionStringRE"].ConnectionString);
                OleDbCommand cmd = con.CreateCommand();
                cmd.CommandText = @"SELECT b.cbpolnum, b.cbstatus, b.cbcovamt, b.cbridnum, b.cbplob FROM olpabddata.lldcb b WHERE  trim(CBPOLNUM)='" + policyNumber + "' AND (b.cbco = '0150') AND ((b.cbridnum = '01') or (b.cbridnum = '02')) Order by b.cbridnum ";
                da = new OleDbDataAdapter(cmd);
                ds = new DataSet();

                try
                {
                    da.Fill(ds);

                    string lob;
                    if (ds.Tables[0].Rows.Count > 1 && ds.Tables[0].Rows.Count != 0)
                    {
                        lob = ds.Tables[0].Rows[0][4].ToString();
                        string lob1 = ds.Tables[0].Rows[1][4].ToString();
                        CoverageAmnt2 = ds.Tables[0].Rows[1][2].ToString();
                        if (lob == "20")
                            PolType = "PA";
                        else if (((lob == "10") || (lob == "40")) && lob1 == "20")
                            PolType = "EMBEDDED";
                        else
                            PolType = "LIFE";
                    }
                    else if (ds.Tables[0].Rows.Count == 1 && ds.Tables[0].Rows.Count != 0)
                    {
                        lob = ds.Tables[0].Rows[0][4].ToString();
                        CoverageAmnt2 = "0";
                        if (lob == "20")
                            PolType = "PA";
                        else
                            PolType = "LIFE";
                    }
                    else
                    {
                        lob = ds.Tables[0].Rows[0][4].ToString();
                        CoverageAmnt2 = "0";
                        if (lob == "20")
                            PolType = "PA";
                        else
                            PolType = "LIFE";
                    }
                }
                catch(Exception)
                {
                }
            }
            catch(Exception)
            {
            }
        }

        public LifelineResponse GetPolicy(string policyNumber)
        {
            try
            {
                try
                {
                    try
                    {
                        Connection.Open();
                    }
                    catch
                    {
                        return LifelineResponse(103);
                    }

                    OleDbCommand cmd1 = Connection.CreateCommand();
                    cmd1.CommandTimeout = 1;
                    cmd1.CommandText = @"SELECT        IFNULL(CT_FLAG, 'I') AS CT_FLAG, IFNULL(trim(CT_POLNUM), 'I') AS ct_polnum
FROM            OLPABDDATA.LLDJUCRI
WHERE        (trim(CT_POLNUM) = trim('" + (policyNumber) + "'))";

                    DataSet ds1 = new DataSet();
                    try
                    {
                        da1 = new OleDbDataAdapter(cmd1);
                        da1.Fill(ds1);
                        if (ds1.Tables[0].Rows.Count > 0)
                        {
                            ctflag = ds1.Tables[0].Rows[0][0].ToString();
                            if (ctflag == "P" || (policyNumber.Substring(0, 2).ToUpper().ToString() == "LT" || policyNumber.Substring(0, 2).ToUpper().ToString() == "ES"))
                                ctflag = "true";
                            else
                                ctflag = "false";
                            flag = 1;
                        }
                        else
                        {
                            ctflag = "false";
                            flag = 0;
                        }
                    }
                    catch
                    {
                        return LifelineResponse(107);
                    }

                    try
                    {
                        OleDbCommand cmd2 = Connection.CreateCommand();
                        cmd2.CommandTimeout = 1;
                        cmd2.CommandText = @"SELECT cabmode,caaprem,casprem,caqprem,camprem from olpabddata.lldca where  (trim(CAPOLNUM)     = trim('" + (policyNumber) + "'))";

                        DataSet ds2 = new DataSet();
                        try
                        {
                            da2 = new OleDbDataAdapter(cmd2);
                            da2.Fill(ds2);
                            if (ds2.Tables[0].Rows.Count > 0)
                            {
                                this.CheckPolicy(policyNumber);
                                cabmode = ds2.Tables[0].Rows[0][0].ToString();
                                if (cabmode == "1")
                                {
                                    premium = ds2.Tables[0].Rows[0][4].ToString();
                                }
                                else if (cabmode == "3")
                                {
                                    premium = ds2.Tables[0].Rows[0][3].ToString();
                                }
                                else if (cabmode == "6")
                                {
                                    premium = ds2.Tables[0].Rows[0][2].ToString();
                                }
                                else if (cabmode == "12")
                                {
                                    premium = ds2.Tables[0].Rows[0][1].ToString();
                                }
                            }
                            else
                            {
                                premium = "0.00";
                            }
                        }
                        catch
                        {
                        }
                    }
                    catch
                    {
                    }

                    OleDbCommand cmd = Connection.CreateCommand();
                    cmd.CommandTimeout = 1;

                    if (flag == 1)
                    {
                        if (PolType == "PA")
                            cmd.CommandText = @"Select x.Status, x.Premiuma as AnnualPremium  ,'" + premium + "' as Premium,trim(z.Plancode) as Plancode,' ' as AgencyName,k.AgentCode,P.agentname ,'0' as CoverageAmnt,' ' as PolicyIssAge,x.InsuredName,' ' as InsuredUCC,z.DOBI,' ' as InsuredGender,' ' as InsuredAge,x.OwnerName,' ' as OwnerUCC,l.DobOwner,z.cbsex,z.cbissage,y.MobileNo,y.Address,y.city,z.PolicyIssueDate,x.Mode,'" + ctflag.ToString() + "' as ct_flag,x.casusamt1 as PaymentPaid,'" + premium + "' as PaymenetAmount,z.cbplob,substr(k.AgentCode,1,4) as BranchCode,'" + PolType + "' as PolicyType,z.CoverageAmnt as CoveragePA from (Select a.cabmode as Mode,-1.0*a.casusamt1 as casusamt1, trim(a.capolnum)PolicyNo ,a.CAINAME as InsuredName,a.CAONAME as OwnerName , a.castatus as Status,Round(((a.caaprem)* (a.cabmode))/12,0) as Premium,caaprem as Premiuma,casprem as Premiums,caqprem as Premiumq,camprem as Premiumm from olpabddata.lldca a where trim(a.CAPOLNUM) =trim('" + (policyNumber) + "') and a.caco='0150') x left join    (Select  trim(b.BCCONTROL)BCCONTROL,(b.BCADDR1||' '||b.BCADDR2) as Address ,b.BCMOBILE as MobileNo,b.BCCITY as City from olpabddata.lldbc b where trim(b.BCCONTROL) =trim('" + (policyNumber) + "') and b.bcco='0150')y  on x.PolicyNo =y.BCCONTROL left join (Select trim(c.CBPOLNUM)CBPOLNUM,(c.cbbthdd||'/'||c.cbbthmm||'/'||c.cbbthcc||c.cbbthyy) DOBI ,(c.cbplanfil||c.cbplan||c.cbrtbk||c.cbrbasis||c.cbplandur) Plancode,(c.cbissdd||'/'||c.cbissmm||'/'||c.cbisscc||c.cbissyy) PolicyIssueDate,c.cbcovamt as CoverageAmnt, c.cbplob,c.cbissage,c.cbsex from olpabddata.lldcb c where trim(c.CBPOLNUM) =trim('" + (policyNumber) + "') and c.cbco='0150' and c.cbridnum='01')z on x.PolicyNo =z.CBPOLNUM left join (Select trim(d.CCPOLNUM)CCPOLNUM, d.ccagent1 as AgentCode from olpabddata.lldcc d where trim(d.CCPOLNUM)=trim('" + (policyNumber) + "')) k on x.PolicyNo =k.CCPOLNUM  left join (select as400_ea_agtname as agentname,as400_ea_agent from OLPABDDATA.lldea )P on k.AgentCode=P.as400_ea_agent left join (select trim(e.COPOLNO)COPOLNO,(e.cobdd||'/'||e.cobmm||'/'||e.cobcc||e.cobyy) DobOwner from olpabddata.lldco e where trim(e.COPOLNO)=trim('" + (policyNumber) + "'))l on x.PolicyNo =l.COPOLNO Fetch First 1 Row Only";
                        else
                            cmd.CommandText = @"Select x.Status, x.Premiuma as AnnualPremium  ,'" + premium + "' as Premium,trim(z.Plancode) as Plancode,' ' as AgencyName,k.AgentCode,P.agentname ,z.CoverageAmnt,' ' as PolicyIssAge,x.InsuredName,' ' as InsuredUCC,z.DOBI,' ' as InsuredGender,' ' as InsuredAge,x.OwnerName,' ' as OwnerUCC,l.DobOwner,z.cbsex,z.cbissage,y.MobileNo,y.Address,y.city,z.PolicyIssueDate,x.Mode,'" + ctflag.ToString() + "' as ct_flag,x.casusamt1 as PaymentPaid,'" + premium + "' as PaymenetAmount,z.cbplob,substr(k.AgentCode,1,4) as BranchCode,'" + PolType + "' as PolicyType,'" + CoverageAmnt2 + "' as CoveragePA from (Select a.cabmode as Mode,-1.0*a.casusamt1 as casusamt1, trim(a.capolnum)PolicyNo ,a.CAINAME as InsuredName,a.CAONAME as OwnerName , a.castatus as Status,Round(((a.caaprem)* (a.cabmode))/12,0) as Premium,caaprem as Premiuma,casprem as Premiums,caqprem as Premiumq,camprem as Premiumm from olpabddata.lldca a where trim(a.CAPOLNUM) =trim('" + (policyNumber) + "') and a.caco='0150') x left join    (Select  trim(b.BCCONTROL)BCCONTROL,(b.BCADDR1||' '||b.BCADDR2) as Address ,b.BCMOBILE as MobileNo,b.BCCITY as City from olpabddata.lldbc b where trim(b.BCCONTROL) =trim('" + (policyNumber) + "') and b.bcco='0150')y  on x.PolicyNo =y.BCCONTROL left join (Select trim(c.CBPOLNUM)CBPOLNUM,(c.cbbthdd||'/'||c.cbbthmm||'/'||c.cbbthcc||c.cbbthyy) DOBI ,(c.cbplanfil||c.cbplan||c.cbrtbk||c.cbrbasis||c.cbplandur) Plancode,(c.cbissdd||'/'||c.cbissmm||'/'||c.cbisscc||c.cbissyy) PolicyIssueDate,c.cbcovamt as CoverageAmnt, c.cbplob,c.cbissage,c.cbsex from olpabddata.lldcb c where trim(c.CBPOLNUM) =trim('" + (policyNumber) + "') and c.cbco='0150' and c.cbridnum='01')z on x.PolicyNo =z.CBPOLNUM left join (Select trim(d.CCPOLNUM)CCPOLNUM, d.ccagent1 as AgentCode from olpabddata.lldcc d where trim(d.CCPOLNUM)=trim('" + (policyNumber) + "')) k on x.PolicyNo =k.CCPOLNUM  left join (select as400_ea_agtname as agentname,as400_ea_agent from OLPABDDATA.lldea )P on k.AgentCode=P.as400_ea_agent left join (select trim(e.COPOLNO)COPOLNO,(e.cobdd||'/'||e.cobmm||'/'||e.cobcc||e.cobyy) DobOwner from olpabddata.lldco e where trim(e.COPOLNO)=trim('" + (policyNumber) + "'))l on x.PolicyNo =l.COPOLNO Fetch First 1 Row Only";
                    }
                    else
                        cmd.CommandText = @"Select x.Status,x.Premiuma as AnnualPremium  ,'" + premium + "' as Premium ,trim(z.Plancode) as Plancode,' ' as AgencyName,k.AgentCode,P.agentname ,z.CoverageAmnt,' ' as PolicyIssAge,x.InsuredName,' ' as InsuredUCC,z.DOBI,' ' as InsuredGender,' ' as InsuredAge,x.OwnerName,' ' as OwnerUCC,l.DobOwner,z.cbsex,z.cbissage,y.MobileNo,y.Address,y.city,z.PolicyIssueDate,x.Mode,'false' as ct_flag,x.casusamt1 as PaymentPaid,'" + premium + "' as PaymenetAmount,z.cbplob,substr(k.AgentCode,1,4) as BranchCode,'" + PolType + "' as PolicyType,'" + CoverageAmnt2 + "' as CoveragePA from (Select a.cabmode as Mode,-1.0*a.casusamt1 as casusamt1, trim(a.capolnum)PolicyNo ,a.CAINAME as InsuredName,a.CAONAME as OwnerName , a.castatus as Status,Round(((a.caaprem)* (a.cabmode))/12,0) as Premium,caaprem as Premiuma,casprem as Premiums,caqprem as Premiumq,camprem as Premiumm from olpabddata.lldca a where trim(a.CAPOLNUM) =trim('" + (policyNumber) + "') and a.caco='0150') x left join    (Select  trim(b.BCCONTROL)BCCONTROL,(b.BCADDR1||' '||b.BCADDR2) as Address ,b.BCMOBILE as MobileNo,b.BCCITY as City from olpabddata.lldbc b where trim(b.BCCONTROL) =trim('" + (policyNumber) + "') and b.bcco='0150')y  on x.PolicyNo =y.BCCONTROL left join (Select trim(c.CBPOLNUM)CBPOLNUM,(c.cbbthdd||'/'||c.cbbthmm||'/'||c.cbbthcc||c.cbbthyy) DOBI ,(c.cbplanfil||c.cbplan||c.cbrtbk||c.cbrbasis||c.cbplandur) Plancode,(c.cbissdd||'/'||c.cbissmm||'/'||c.cbisscc||c.cbissyy) PolicyIssueDate,c.cbcovamt as CoverageAmnt, c.cbplob,c.cbissage,c.cbsex from olpabddata.lldcb c where trim(c.CBPOLNUM) =trim('" + (policyNumber) + "') and c.cbco='0150' and c.cbridnum='01')z on x.PolicyNo =z.CBPOLNUM left join (Select trim(d.CCPOLNUM)CCPOLNUM, d.ccagent1 as AgentCode from olpabddata.lldcc d where trim(d.CCPOLNUM)=trim('" + (policyNumber) + "')) k on x.PolicyNo =k.CCPOLNUM  left join (select as400_ea_agtname as agentname,as400_ea_agent from OLPABDDATA.lldea )P on k.AgentCode=P.as400_ea_agent left join (select trim(e.COPOLNO)COPOLNO,(e.cobdd||'/'||e.cobmm||'/'||e.cobcc||e.cobyy) DobOwner from olpabddata.lldco e where trim(e.COPOLNO)=trim('" + (policyNumber) + "'))l on x.PolicyNo =l.COPOLNO Fetch First 1 Row Only";

                    ds = new DataSet();
                    try
                    {
                        da = new OleDbDataAdapter(cmd);
                        da.Fill(ds);
                    }
                    catch (TimeoutException)
                    {
                        return LifelineResponse(103);
                    }
                    catch
                    {
                        return LifelineResponse(107);
                    }

                    Connection.Close();

                    if (ds.Tables[0].Rows.Count <= 0)
                    {
                        cmd.CommandText = @" 
              select trim(FRPOLNUM) as PolicyNo,' ' as STATUS,' ' as ANNUALPREMIUM,' ' as  PREMIUM,' ' as PLANCODE,' ' as AGENCYNAME, ' ' as AGENTCODE, ' ' as AGENTNAME, ' ' as COVERAGEAMNT, ' ' as POLICYISSAGE,
' ' as INSUREDNAME, ' ' as INSUREDUCC, ' ' as DOBI, ' ' as INSUREDGENDER, ' ' as INSUREDAGE, ' ' as OWNERNAME,' ' as OWNERUCC, ' ' as CBSEX, ' ' as CBISSAGE, ' ' as MOBILENO, ' ' as ADDRESS,' ' as CITY,' ' as POLICYISSUEDATE,
 ' ' as MODE,' ' as CT_FLAG, ' ' as PAYMENTPAID, ' ' as PAYMENETAMOUNT,
' ' as CBPLOB, FRAGENCY as BRANCHCODE,'' as PolicyType, '' as CoveragePa
from OLPABDDATA.llbform  where trim(FRPOLNUM)=trim('" + (policyNumber) + "')";

                        ds = new DataSet();
                        try
                        {
                            da = new OleDbDataAdapter(cmd);
                            da.Fill(ds);
                        }
                        catch
                        {
                            return LifelineResponse(107);
                        }

                        if (ds.Tables[0].Rows.Count <= 0)
                        {
                            //return ds.GetXml().Insert(0, Error(109));
                            return LifelineResponse(1, true, ds.Tables[0]);
                        }
                    }

                    return LifelineResponse(1, true, ds.Tables[0]);
                }
                catch (Exception)
                {
                    return LifelineResponse(105);
                }
            }
            catch (Exception)
            {
                return LifelineResponse(104);
            }
        }
    }
}