﻿namespace WebApp.Client
{
    public class LifelineResponse
    {
        public string StatusCode { get; set; }
        public string Status { get; set; }
        public object Data { get; set; }
    }
}