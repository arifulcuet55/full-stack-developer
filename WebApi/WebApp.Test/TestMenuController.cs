﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApp.Domain.Entities;
using WebApp.Controllers;
using System.Web.Http.Results;

namespace WebApp.Test
{
    [TestClass]
    public class TestMenuController
    {

        private List<Menu> GetMenus()
        {
            var menus = new List<Menu>();

            menus.Add(new Menu { Id = 1, Name = "Menu-1", ComponentName = "C-1", Domain = "", Url = "" });
            menus.Add(new Menu { Id = 2, Name = "Menu-2", ComponentName = "C-2", Domain = "", Url = "" });
            menus.Add(new Menu { Id = 3, Name = "Menu-3", ComponentName = "C-3", Domain = "", Url = "" });

            return menus;
        }

        [TestMethod]
        public async Task GetByIdAsync_ShouldReturnSpecficMenu()
        {
            //var controller = new MenuController();

            //var obj = new { data = new Menu() };
            //var result = await controller.GetByIdAsync(1) as OkNegotiatedContentResult<Menu>;
            //Assert.AreEqual(1, 1);
        }
    }
}
