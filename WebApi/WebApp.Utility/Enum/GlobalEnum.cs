﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp.Utility.Enums
{

    public enum FilePath
    {
        ProfilePic
    }
    public enum MenuStatus : int
    {
        Active = 1,
        Inactive = 2
    }
    public enum ItemStatus : int
    {
        Inprogress = 1,
        Done = 2
    }
}
