﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace WebApp.Utility
{
    public static class FileHandler
    {
        public static string[] ExcelFileType = { ".xlsx", ".xls", ".csv" };
        public static string[] ImageFileType = { ".png", ".bmp", ".jpg", ".jpeg" };
        public static string[] PdfType = { ".pdf" };
        public static float MaxFileSize = 5; // size in MB  
        private static string defaultRoot = "~/Assets";

        public static string SaveFile(HttpPostedFile file, string filepath, string fileName = "")
        {
            string physical_path = "", path = "";

            try
            {
                 filepath = filepath.Contains(defaultRoot) ? filepath.Replace(defaultRoot, "") : filepath;
                 var tpl = fileName == "" ? GenerateFilePath(file, filepath) : GenerateCustomFilePath(file, filepath, fileName);

                physical_path = HttpContext.Current.Server.MapPath(tpl.filePath);
                WriteFile(file, physical_path);
                return tpl.filePath.Replace("~", "");
            }
            catch (Exception ex)
            {
                System.IO.File.CreateText(path).WriteLine(ex.ToString());
                throw;
            }
        }
        private static bool WriteFile(HttpPostedFile file, string physicalPath)
        {
            bool success;
            try
            {

                if (string.IsNullOrWhiteSpace(physicalPath))
                {
                    throw new Exception("File path not valid.");
                }

                if (physicalPath.Contains(".."))
                {
                    throw new Exception("File path not valid.");
                }
                string fileName = Path.GetFileNameWithoutExtension(physicalPath);
                string fileExtension = Path.GetExtension(physicalPath);

                if (Path.GetInvalidFileNameChars().Any(c => fileName.Contains(c)))
                {
                    throw new Exception("Invalid file name.");
                }

                //if (!string.IsNullOrWhiteSpace(fileExtension)
                //    && !Path.GetInvalidFileNameChars()
                //        .Any(c => fileName.Contains(c)))
                //{
                //    var allow = SessionManager.GetAllowFileExt;
                //    if (!allow.Contains(fileExtension.ToLower()))
                //    {
                //        throw new Exception("File not valid.");
                //    }
                //}
                //else
                //{
                //    throw new Exception("File not valid.");
                //}

                file.SaveAs(physicalPath);

                success = true;
            }
            catch (Exception e)
            {
                throw;
            }

            return success;
        }

        public static (string fileName, string filePath) GenerateFilePath(HttpPostedFile file, string filepath)
        {
            string file_name = string.Empty;
            string default_root = defaultRoot + ((!string.IsNullOrEmpty(filepath)) ? "/" + filepath : "");

            var has_directory = Directory.Exists(HttpContext.Current.Server.MapPath(default_root));
            if (!has_directory)
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(default_root));

            file_name = Guid.NewGuid() + "_" + Path.GetFileNameWithoutExtension(file.FileName) + Path.GetExtension(file.FileName);
            default_root += "/" + file_name;

            (string fileName, string rootPath) tpl = (file_name, default_root);
            return tpl;
        }

        public static bool SaveFile(this byte[] value, string filePath, string expectedExtension)
        {
            bool success;
            expectedExtension = expectedExtension.ToLower();
            try
            {

                if (string.IsNullOrWhiteSpace(filePath))
                {
                    throw new Exception("File path not valid.");
                }

                if (filePath.Contains(".."))
                {
                    throw new Exception("File path not valid.");
                }
                string fileName = Path.GetFileNameWithoutExtension(filePath);
                string fileExtension = Path.GetExtension(filePath);

                if (Path.GetInvalidFileNameChars().Any(c => fileName.Contains(c)))
                {
                    throw new Exception("Invalid file name.");
                }

                if (!string.IsNullOrWhiteSpace(fileExtension)
                    && !Path.GetInvalidFileNameChars()
                        .Any(c => fileName.Contains(c)))
                {
                    var allow = PdfType;
                    if (!allow.Contains(fileExtension.ToLower()) || !expectedExtension.Contains(fileExtension.ToLower()))
                    {
                        throw new Exception("File not valid.");
                    }
                }
                else
                {
                    throw new Exception("File not valid.");
                }

                using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(value, 0, value.Length);
                }

                success = true;
            }
            catch (Exception)
            {
                throw;
            }

            return success;
        }

        public static (string fileName, string filePath) GenerateCustomFilePath(HttpPostedFile file, string filepath, string fileName)
        {
            string file_name = string.Empty;
            string default_root = defaultRoot + ((!string.IsNullOrEmpty(filepath)) ? "/" + filepath : "");

            var has_directory = Directory.Exists(HttpContext.Current.Server.MapPath(default_root));
            if (!has_directory)
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(default_root));

            file_name = fileName + Path.GetExtension(file.FileName);
            default_root += "/" + file_name;

            (string fileName, string rootPath) tpl = (file_name, default_root);
            return tpl;
        }

        public static void RemoveFile(string path)
        {

            try
            {
                if (!string.IsNullOrEmpty(path))
                {
                    bool IsProd = ConfigurationSettings.IsProductionEnv;
                    string uri_default = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "");

                    string uri = ConfigurationSettings.GetEndPointUrl;

                    path = path.Replace(((IsProd) ? uri : uri_default), "~");

                    var physical_path = HttpContext.Current.Server.MapPath(path);
                    if (System.IO.File.Exists(physical_path))
                    {
                        System.IO.File.Delete(physical_path);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
