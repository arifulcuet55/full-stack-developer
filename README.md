# Fullstack Developer Challenge

## Project Demo
[Demo Link](http://3.0.155.79:8070/clientApp/)

## Technology
* Angular 8 for frontend
* Asp.Net WebApi for backend
* Sql Server 2016 for Database

## How is it work?
1. Clone project 
2. Go to App folder install NPM   `npm install` and run `ng serve`
3. Go to WebApi and open the sln with visual stadion 2019  `WebApp.sln`

## How To Participate
1. Fork this repo
2. Do your best
3. Prepare pull request and let us know that you are done

## Requirements
* Use NodeJS or language of your choice for backend. 
* Use JS framework (Vue, React, Angular).
* Use CSS preprocessor (SCSS, SASS, LESS).
* Use MySQL or other SQL Database for data store.
* Use task-runner, to compile your css/js (Gulp, Webpack or Grunt), but do not commit the build.

## Some things we love
* API-first approach
* Structure
* Comments
* Meaningful methods
* Fun / Magic / Creativity!