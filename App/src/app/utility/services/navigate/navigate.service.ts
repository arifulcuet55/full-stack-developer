import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { RouteUrl } from '../../configuration/routeUrl';

@Injectable({
  providedIn: 'root'
})
export class NavigateService {
  url = RouteUrl;

  constructor(private router: Router) { }

  toHome() {
    this.router.navigate([this.url.home]);
  }

  toLogin() {
    this.router.navigate([this.url.login]);
  }

  toLogout() {
    this.router.navigate([this.url.logout]);
  }

  toSignUp() {
    this.router.navigate([this.url.signup]);
  }

  toAddNewItem(id: any) {
    this.router.navigate([this.url.newItem,id]);
  }

  toChangePassword() {
    this.router.navigate([this.url.changePassword]);
  }

  toForgetPassword() {
    this.router.navigate([this.url.forgetPassword]);
  }

}
