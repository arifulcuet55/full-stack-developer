import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class TimeService {

  constructor() { }
  
   tConvert (time) {
     const splitted =  time.split(":");
    // Check correct time format and split into components
    time = splitted[0]+":"+splitted[1];
    // time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    const hour = splitted[0] >= 12 ? 'pm' : 'am';
    return time+' '+hour // return adjusted time or original string
  }

  getUtcTime(date: any){
    return date.replace(/^[^:]*([0-2]\d:[0-5]\d).*$/, "$1");
  }
}
