import { Injectable } from '@angular/core';
import { MessageService } from '../message/message.service';

@Injectable({
  providedIn: 'root'
})
export class FileHandlerService {

  private fileOptions: IFileOptions;
  constructor(public messageService: MessageService) { }

  validator(file: File, options: IFileOptions) {
    if (file != null) {
      const ext = file.name.substring(file.name.lastIndexOf('.') + 1, file.name.length);
      const size = file.size / 1024;  // file in kb
      this.fileOptions = options;

      if ( this.fileOptions.acctptedTypes &&
      (!this.fileOptions.acctptedTypes.split(',').includes(ext) ||
      (this.fileOptions.hasOwnProperty('maxSizeKB') && this.fileOptions.maxSizeKB < size))) {
        this.showMessage();
        return false;
      } else {
        return true;
      }
    } else {
      this.messageService.show(this.messageService.msgFileNotFound, 'warning');
      return false;
    }
  }



  private showMessage() {
    let sizeError = '';
    if (this.fileOptions.hasOwnProperty('maxSizeKB')) {
      sizeError = ` & max ${this.fileOptions.maxSizeKB} KB size of file are supported`;
    }
    const message = `File type should be ${this.fileOptions.acctptedTypes}${sizeError}.`;
    this.messageService.show(message, 'warning');
  }

}

export interface IFileOptions {
  acctptedTypes: string;
  maxSizeKB?: number;
  validators?: any[];
  validatorKey?: string;
}


