import { Injectable } from "@angular/core";
import Swal, { SweetAlertType } from "sweetalert2";

@Injectable({
  providedIn: "root"
})
export class MessageService {
  msgSave = "Save Successfully";
  msgUpdate = "Update Successfully";
  msgDelete = "Deleted Successfully";
  msgNotfound = "Data not found";
  msgConfirmDelete = "Are you sure to delete";
  forgetPassword = "Ohh!, It is under developing!!";
  msgFileNotFound="File not found";
  resetSuccess = "Password reset successfully.";
  rejectConfirmation = "Are you sure you want to reject?";
  updateStatus = "Are you sure you want to update status?";
  warningTime = "From time must be smaller than to time.";

  constructor() {}

  show(message: string, type: SweetAlertType) {
    Swal.fire({
      type,
      text: message,
      showConfirmButton: false,
      showCloseButton: true,
      //showCancelButton: true
       timer: 1500
    });
  }

  confirm(message?: string) {
    return Swal.fire({
      title: message ? message : "Are you sure?",
      type: "warning",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      reverseButtons: true,
      showCloseButton: true
    });
  }
}
