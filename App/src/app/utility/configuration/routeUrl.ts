export const RouteUrl = {
  home: '/home',
  signup: '/signup',
  login: '',
  logout: '',
  newItem: '/schedule/item-create',
  changePassword: '/auth/change-password',
  forgetPassword: '/auth/forget-password',
};
