export enum GlobalImage {
    ProfileDefaultImg = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/20625/avatar-bg.png',
}
export enum ItemStatus {
    Inprogress = 1,
    Done = 2
}