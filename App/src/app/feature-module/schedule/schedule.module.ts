import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateScheduleComponent } from './create-schedule/create-schedule.component';
import { RoutingModule } from './routing.module';
import { NgmatModule } from 'src/app/shared-module/ngmat/ngmat.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CreateScheduleComponent],
  imports: [
    CommonModule,
    RoutingModule,
    NgmatModule,
    ReactiveFormsModule
  ]
})
export class ScheduleModule { }
