import { NgModule } from '@angular/core';
import { RouterModule,Route } from '@angular/router';
import { CreateScheduleComponent } from './create-schedule/create-schedule.component';


var routes: Route[] = [
    {
      path: 'item-create/:id',
      component: CreateScheduleComponent,
    },
    {
      path: 'item-create',
      component: CreateScheduleComponent,
    }
  ]

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class RoutingModule { }