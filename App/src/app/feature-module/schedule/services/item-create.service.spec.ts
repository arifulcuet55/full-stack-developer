import { TestBed } from '@angular/core/testing';

import { ItemCreateService } from './item-create.service';

describe('ItemCreateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItemCreateService = TestBed.get(ItemCreateService);
    expect(service).toBeTruthy();
  });
});
