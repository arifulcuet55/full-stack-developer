import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'src/app/utility/services/message/message.service';
import { ItemCreateService } from '../services/item-create.service';
import { ActivatedRoute } from '@angular/router';
import { NavigateService } from 'src/app/utility/services/navigate/navigate.service';

@Component({
  selector: 'app-create-schedule',
  templateUrl: './create-schedule.component.html',
  styleUrls: ['./create-schedule.component.scss']
})
export class CreateScheduleComponent implements OnInit {

  notityList = [
    {
      id: 1,
      name: 10
    },
    {
      id: 2,
      name: 20
    },
    {
      id: 3,
      name: 30
    }]

    minDate = new Date();
  constructor(
    private fb: FormBuilder,
    private messageService: MessageService,
    private itemCreateService: ItemCreateService,
    private route: ActivatedRoute,
    private navigateService: NavigateService
  ) { }

  itemCreateFrom = this.fb.group({
    id: [null],
    title: [null, [Validators.required]],
    description: [null, [Validators.required]],
    date: [new Date(), [Validators.required]],
    from: [null, [Validators.required]],
    to: [null, [Validators.required]],
    location: [null, [Validators.required]],
    notify: [null, [Validators.required]],
    label: [null, [Validators.required]],
    isActive: [null],
    createdBy: [null],
    createdDateUtc: [null],
    updatedBy: [null],
    updatedDateUtc: [null],
    statusId: [null],
    isOutOfDate:[null]
  });

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.getById(id);
    }
  }

  getById(id: any) {
    this.itemCreateService.getById(id).subscribe(res => {
      this.itemCreateFrom.setValue(res);
    },
      err => {

        this.messageService.show(err.error.message, 'error');
      });
  }

  submit() {
    debugger;
    const from = this.itemCreateFrom.controls['from'].value;
    const to = this.itemCreateFrom.controls['to'].value;
    if(from> to){
      this.messageService.show(this.messageService.warningTime, 'warning');
      return;
    }
    let value = this.itemCreateFrom.value;
    value.date = new Date(value.date).toLocaleDateString();
      if (this.itemCreateFrom.controls['id'].value > 0) {
        this.itemCreateService.edit(this.itemCreateFrom.controls['id'].value,value ).subscribe(res => {
          this.navigateService.toHome();
          //this.messageService.show(this.messageService.msgSave, 'success');
        },
          err => {

            this.messageService.show(err.error.message, 'error');
          });
      }
      else {
        this.itemCreateService.add(this.itemCreateFrom.value).subscribe(res => {
          this.navigateService.toHome();
         //this.messageService.show(this.messageService.msgSave, 'success');
        },
          err => {

            this.messageService.show(err.error.message, 'error');
          });
      }

  }
  backHome() {
    this.navigateService.toHome();
  }

}
