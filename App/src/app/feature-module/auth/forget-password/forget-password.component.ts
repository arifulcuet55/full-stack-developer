import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, Validators, FormBuilder } from '@angular/forms';
import { MessageService } from 'src/app/utility/services/message/message.service';
import { NavigateService } from 'src/app/utility/services/navigate/navigate.service';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  timeLeft = 70;
  subscribeTimer: any;
  forgetPasswordForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    token: ['', [Validators.required]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    confirmPassword: ['', [Validators.required]],
  });
  isOTPSend: boolean = false;
  isOTPExpired: boolean = false;
  message: string = "";

  constructor(private fb: FormBuilder,
    private messageService: MessageService,) { }

  ngOnInit() {
  }
  submit(){
    this.messageService.show(this.messageService.forgetPassword,'warning')
  }
}
