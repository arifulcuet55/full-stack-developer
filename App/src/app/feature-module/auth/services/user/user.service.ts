import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject } from "rxjs";
import { NavigateService } from "src/app/utility/services/navigate/navigate.service";


@Injectable({
  providedIn: "root"
})
export class UserService {
  routePrefix = "/api/accounts";

  constructor(private http: HttpClient, private navigate: NavigateService) {}

  gets(): Observable<any> {
    return this.http.get(this.routePrefix + "/gets");
  }

  getById(id: number): Observable<any> {
    return this.http.get(this.routePrefix + "/getById" + "/" + id);
  }

  edit(id: number, data, roles: any[]): Observable<any> {
    data.roles = roles;
    return this.http.put(this.routePrefix + "/edit/" + id, data);
  }

  changePassword(data): Observable<any> {
    return this.http.put(this.routePrefix + "/changePassword/", data);
  }

  resetPassword(data): Observable<any> {
    return this.http.post(this.routePrefix + "/passwordResetByOTP/", data);
  }

}
