import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { MessageService } from 'src/app/utility/services/message/message.service';
import { NavigateService } from 'src/app/utility/services/navigate/navigate.service';
import { StorageService } from 'src/app/utility/services/storage/storage.service';
import { IFileOptions, FileHandlerService } from 'src/app/utility/services/file-handler/file-handler.service';
import { GlobalImage } from 'src/app/utility/enum/global-enum';



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  fileOption: IFileOptions = {
    acctptedTypes: 'png,jpg,PNG',
    maxSizeKB: 2048,
    validators: ['png', 'jpg', 'PNG']
  };
  file: File
  authReg = this.fb.group({});
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private storageService: StorageService,
    private navigate: NavigateService,
    private messageService: MessageService,
    private fileHandlerService: FileHandlerService,

  ) { }

  ngOnInit() {
    if(!this.authService.IsTokenExpire()){
          this.navigate.toHome();
    }
    this.authReg = this.fb.group({
      name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      birthDate: ['', [Validators.required]],
      previewUrl: [GlobalImage.ProfileDefaultImg]
    });
  }

  submit() {
    this.authService.signup(this.getFormData(this.authReg.value)).subscribe(res => {
      this.authService.signin(this.authReg.controls['email'].value, this.authReg.controls['password'].value);
    },
      err => {

        this.messageService.show(err.error.message, 'error');
      });
  }
  signin() {
    this.navigate.toLogin();
  }
  onFileChange(event) {
    if (event && this.fileHandlerService.validator(event.target.files[0], this.fileOption)) {
      this.file = event.target.files[0];
      this.preview();
    }

  }
  preview() {
    // Show preview 
    var mimeType = this.file.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.file);
    reader.onload = (_event) => {
      this.authReg.patchValue({
        previewUrl: reader.result
      })
    }
  }

  getFormData(entity) {
    var formData = new FormData();
    formData.append('model', JSON.stringify(entity));
    formData.append('image', this.file);
    return formData;
  }


}
