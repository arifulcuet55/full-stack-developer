import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MinuteConversionPipe } from 'src/app/utility/pipe/minute-conversion/minute-conversion.pipe';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material';

@NgModule({
  declarations: [
    MinuteConversionPipe, 

  ],
  imports: [
    CommonModule
  ],
  exports:[
    MinuteConversionPipe,

  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}
  ]
})
export class CustomModule { }