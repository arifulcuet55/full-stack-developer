import { Component } from "@angular/core";
import { AuthService } from "./feature-module/auth/services/auth.service";
import { NavigateService } from "./utility/services/navigate/navigate.service";
import { StorageService } from "./utility/services/storage/storage.service";
import { environment } from 'src/environments/environment';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "claims-payment";
  menus: any = [
    {
      Name: 'Home',
      Url: '/home'
    },
    {
      Name: 'New Item',
      Url: '/schedule/item-create',
    },
    {
      Name: 'Change Password',
      Url: '/auth/change-password'
    },]
  IsLogIn = false;
  name: string;
  email: string;
  profileUrl: string;
  baseUrl = environment.endPointUrl;
  constructor(
    private authService: AuthService,
    private navigateService: NavigateService,
    private storageService: StorageService
  ) {
    this.authService.getAuthenitication().subscribe(res => {
      this.IsLogIn = res;
      this.name = "";
      if (this.IsLogIn) {
        this.name = this.storageService.getItem("name");
        this.email = this.storageService.getItem("email");
        this.profileUrl = this.storageService.getItem("profileUrl");
      }
    });
  }

  logOut() {
    this.authService.signout();
  }
  changePassword() {
    this.navigateService.toChangePassword();
  }
}
