import { Component, OnInit, ViewChild } from '@angular/core';
import { ItemCreateService } from 'src/app/feature-module/schedule/services/item-create.service';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { NavigateService } from 'src/app/utility/services/navigate/navigate.service';
import $ from 'jquery'
import { Time } from '@angular/common';
import { MessageService } from 'src/app/utility/services/message/message.service';
import { ItemStatus } from 'src/app/utility/enum/global-enum';
import { TimeService } from 'src/app/utility/services/timeConvertion/time.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  step = 0;
  itemList: any;
  previousDate = null;
  displayedColumns: string[] = ['shedule'];
  dataSource: any;
  isExpanded = false;
  isDataLoad = true;
  itemStatusEnum = ItemStatus;
  today = new Date();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(
    private itemCreateService: ItemCreateService,
    private navigateService: NavigateService,
    private timeService: TimeService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getAll();
  }

  getAll() {
    this.itemCreateService.gets().subscribe(res => {
      this.itemList = res;

      this.itemList.forEach(element => {
        element.isExpanded = false;
      });
      this.dataSource = new MatTableDataSource(this.itemList);
      this.dataSource.paginator = this.paginator;
    },
      err => {

        //this.messageService.show(err.error.message, 'error');
      });
  }
  checkDate(date: Date) {
    var date1 = new Date(date);
    if (this.previousDate == null) {
      this.previousDate = this.previousDate == null ? date1 : this.previousDate;
      return true;
    }

    else if (date1.getDate() != this.previousDate.getDate() ||
      date1.getFullYear() !== this.previousDate.getFullYear() ||
      date1.getMonth() != this.previousDate.getMonth()) {
      this.previousDate = date1;
      return true;
    }
    else {
      return false;
    }
  }

  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  setBorderStyle() {
    let styles = {
      'border-left': '5x solid ' + this.getRandomColor()
    };
    debugger;
    return styles;
  }
  addNew() {
    this.navigateService.toAddNewItem('');
  }
  changeEvent(item) {
    this.isExpanded = $('#headingOne' + item.id).attr("aria-expanded");
    item.isExpanded = this.isExpanded;
  }
  edit(item: any) {
    this.navigateService.toAddNewItem(item.id);
  }
  tConvert(time: any) {
    return this.timeService.tConvert(time);
  }

  delete(id: number) {
    this.messageService.confirm(this.messageService.msgConfirmDelete).then(res => {
      if (res.value) {
        this.itemCreateService.delete(id).subscribe(res => {
          this.getAll();
          this.messageService.show(this.messageService.msgDelete, 'success');
        },
          err => {
            this.messageService.show(err.error.message, 'error');
          });
      }
    })

  }
  
  getColor(item: any){

    if(  !item.isOutOfDate  || item.statusId==ItemStatus.Done){
     
          return 'lightgray';
    }
     return 'black';
  }

  statusUpdate(id: any){
    this.messageService.confirm(this.messageService.updateStatus).then(res => {
      if (res.value) {
        this.itemCreateService.statusUpdate(id).subscribe(res => {
          this.getAll();
          this.messageService.show(this.messageService.msgUpdate, 'success');
        },
          err => {
            this.messageService.show(err.error.message, 'error');
          });
      }
    })

  }

}
